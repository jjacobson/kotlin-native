FROM openjdk:9-slim

ARG KOTLIN_NATIVE_VERSION=0.4

RUN set -ex && \
    apt-get update -yqq && \
    apt-get install -yqq --no-install-recommends \
        bash \
        curl && \
    curl -sSL -o kotlin-native-linux-${KOTLIN_NATIVE_VERSION}.tar.gz https://github.com/JetBrains/kotlin-native/releases/download/v${KOTLIN_NATIVE_VERSION}/kotlin-native-linux-${KOTLIN_NATIVE_VERSION}.tar.gz && \
    tar xzf kotlin-native-linux-${KOTLIN_NATIVE_VERSION}.tar.gz && \
    rm -rf kotlin-native-linux-${KOTLIN_NATIVE_VERSION}.tar.gz && \
    mv kotlin-native-linux-${KOTLIN_NATIVE_VERSION} /usr/local/lib/kotlin-native && \
    export PATH="$PATH:/usr/local/lib/kotlin-native/bin" && \
    kotlinc --check_dependencies && \
    rm -rf /var/lib/apt/lists/*

ENV PATH="$PATH:/usr/local/lib/kotlin-native/bin"
